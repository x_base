#!/usr/bin/env wmii9rc
# WMII Configuration

oldpath=$path
path=($PLAN9/bin $path)

MODKEY=Mod4
UP=k
DOWN=j
LEFT=h
RIGHT=l

WMII_FONT='fixed'
WMII_NORMCOLORS=('#222222' '#5FBF77' '#2A7F3F')
WMII_FOCUSCOLORS=('#ffffff' '#153F1F' '#2A7F3F')
WMII_BACKGROUND='#333333'

WMII_TERM=(xterm)

fn wmiimenu {
	dmenu -b -fn $WMII_FONT \
		-nf $WMII_NORMCOLORS(1) -nb $WMII_NORMCOLORS(2) \
		-sf $WMII_FOCUSCOLORS(1) -sb $WMII_FOCUSCOLORS(2)
}

fn 9menu {
	wmii9menu -font $WMII_FONT \
		-^(nf nb br)^$WMII_NORMCOLORS \
		-^(sf sb br)^$WMII_FOCUSCOLORS $*
}

fn config_whatis {
	confpath=`{echo $WMII_CONFPATH | sed 'y/:/ /'}
	prog=$1; shift
	echo `{{path=$confpath whatis $prog} | grep -v '=|^fn '} $*
}

if(echo $0 | grep -vs '/rc.wmii$') {
	echo Fatal: This file should only be run as rc.wmii >[1=2]
	exit badname
}

if(! test -x $PLAN9/bin/read) {
	echo 'Can''t find the ''read'' command' >[1=2]
	xmessage -file - <<'!'
	exec `{config_whatis wmiirc}
}
rc.wmii can't run:
	You have a Plan 9 utilities installed, but are missing the 'read' command.
	This likely means that you have an out-of-date 9base installed.

wmiirc will be run instead.
!

# Source Variables, &c
local = `{config_whatis rc.wmii.local}
. <{awk '//; /^# Overrides/ { exit }' $local </dev/null}

# Column Rules
wmiir write /colrules <<!
/./ -> 60+40
!

# Tagging Rules
wmiir write /tagrules <<!
/XMMS.*/ -> ~
/MPlayer.*/ -> ~
/.*/ -> !
/.*/ -> 1
!

# Status Bar Info
fn status {
	echo -n `{uptime | sed 's/.*://; s/,//g'} '|' `{date}
}

# View Titles
fn viewtitle { echo $* }

# Events
fn Event-Start {
	switch($1) {
	case wmiirc
		rm -f $progs_file
		exit
	}
}

fn Event-Key { eval Key-$1 $1 }

fn Event-CreateTag { echo $WMII_NORMCOLORS `{viewtitle $*} | wmiir create /lbar/$"* }
fn Event-DestroyTag { wmiir remove /lbar/$"* }
fn Event-FocusTag { wmiir xwrite /lbar/$"* $WMII_FOCUSCOLORS `{viewtitle $*} }
fn Event-UnfocusTag { wmiir xwrite /lbar/$"* $WMII_NORMCOLORS `{viewtitle $*} }
fn Event-UrgentTag { shift; wmiir xwrite /lbar/$"* '*'$"* }
fn Event-NotUrgentTag { shift; wmiir xwrite /lbar/$"* $"* }

fn Event-LeftBarClick { shift; wmiir xwrite /ctl view $* }

menulast = ''
fn Event-ClientMouseDown {
	client = $1; button = $2
	if(~ $button 3) {
		do=`{9menu -initial $menulast Nop Delete}
		switch($do) {
		case Delete
			wmiir xwrite /client/$client/ctl kill
		}
		if(! ~ $#do 0)
			menulast = $do;
	}
}

# Actions
fn Action {
	action=$1; shift
	if(whatis Action-$action | grep -s '^fn ') {
		Action-$action $* &
	};if not {
		run_command `{config_whatis $action} $* &
	}
}
fn Action-rehash { proglist $PATH >$progs_file }
fn Action-quit { wmiir xwrite /ctl quit }
fn Action-status {
	flag x -
	flag r -
	if(wmiir remove /rbar/status >[2]/dev/null)
		sleep 2
	echo $WMII_NORMCOLORS | wmiir create /rbar/status
	while(status | wmiir write /rbar/status)
		sleep 1
}

# Key Bindings
fn Key-$MODKEY-Control-t {
	switch(`{wmiir read /keys | wc -l}) {
	case 0 1
		initkeys
		wmiir xwrite /ctl grabmod $MODKEY
	case *
		wmiir xwrite /keys $MODKEY-Control-t
		wmiir xwrite /ctl grabmod Mod3
	}
}

fn Key-$MODKEY-$LEFT { wmiir xwrite /tag/sel/ctl select left }
fn Key-$MODKEY-$RIGHT { wmiir xwrite /tag/sel/ctl select right }
fn Key-$MODKEY-$DOWN { wmiir xwrite /tag/sel/ctl select down }
fn Key-$MODKEY-$UP { wmiir xwrite /tag/sel/ctl select up }

fn Key-$MODKEY-Shift-$LEFT { wmiir xwrite /tag/sel/ctl send sel left }
fn Key-$MODKEY-Shift-$RIGHT { wmiir xwrite /tag/sel/ctl send sel right }
fn Key-$MODKEY-Shift-$DOWN { wmiir xwrite /tag/sel/ctl send sel down }
fn Key-$MODKEY-Shift-$UP { wmiir xwrite /tag/sel/ctl send sel up }

fn Key-$MODKEY-space { wmiir xwrite /tag/sel/ctl select toggle }
fn Key-$MODKEY-Shift-space { wmiir xwrite /tag/sel/ctl send sel toggle }

fn Key-$MODKEY-d { wmiir xwrite /tag/sel/ctl colmode sel default }
fn Key-$MODKEY-s { wmiir xwrite /tag/sel/ctl colmode sel stack }
fn Key-$MODKEY-m { wmiir xwrite /tag/sel/ctl colmode sel max }

fn Key-$MODKEY-Shift-c { wmiir xwrite /client/sel/ctl kill }

fn Key-$MODKEY-a { Action `{actionlist | wmiimenu} & }
fn Key-$MODKEY-p { run_command `{wmiimenu <$progs_file} & }
fn Key-$MODKEY-Return { run_command $WMII_TERM & }
fn Key-$MODKEY-t { wmiir xwrite /ctl view `{read_tags | wmiimenu} & }
fn Key-$MODKEY-Shift-t {
	wmiir xwrite /client/`{wmiir read /client/sel/ctl}^/tags `{read_tags | wmiimenu} &
}

for(i in `{seq 0 9}) {
	fn Key-$MODKEY-$i { wmiir xwrite /ctl view `{echo $1 | sed 's/.*-//'} }
	fn Key-Shift-$MODKEY-$i {wmiir xwrite /client/sel/tags `{echo $1 | sed 's/.*-//'} }
}

# Functions
fn proglist {
        /bin/ls -lL `{echo $* | sed 'y/:/ /'} >[2]/dev/null \
		| awk '$1 ~ /^[^d].*x/ { print $NF }' \
		| sort | uniq
}

fn getfuns {
	env | sed -n 's/^fn#'^$1^'-([^=]+).*/\1/p'
}

fn actionlist {
	{	proglist $WMII_CONFPATH
	 	getfuns Action
	} | sort | uniq
}

fn initkeys {
	getfuns Key | wmiir write /keys
}

fn read_tags {
	wmiir ls /tag | sed 's,/,,; /^sel$/d'
}

fn run_command {
	@{	rfork ns
		path=$oldpath
		eval exec $*
	}
}

# WM Configuration
wmiir write /ctl <<!
grabmod $MODKEY
border 2
font $WMII_FONT
focuscolors $WMII_FOCUSCOLORS
normcolors $WMII_NORMCOLORS
!
xsetroot -solid $WMII_BACKGROUND

# Source Overrides
. <{awk '/^# Overrides/, 0' $local </dev/null}

# Misc Setup
Action status &
progs_file=$WMII_NS_DIR/proglist.$pid
proglist $PATH >$progs_file &

# Tag Bar Setup
ifs='#
'{	for(bar in `{comm -23 <{wmiir ls /lbar} <{read_tags}})
		wmiir remove /lbar/$bar
	seltag=`{wmiir read /tag/sel/ctl}
	for(tag in `{read_tags}) {
		if(~ $tag $seltag)
			echo $WMII_FOCUSCOLORS `{viewtitle $tag} | wmiir create /lbar/$tag
		if not
			echo $WMII_NORMCOLORS `{viewtitle $tag} | wmiir create /lbar/$tag
	}
}

# Keygrab Setup
initkeys

if(echo Start wmiirc | ! wmiir write /event >[2]/dev/null)
	exit 1

# Event Loop
wmiir read /event |
	while(*=`{read}) {
		event = $1; shift
		Event-$event $*
	} >[2]/dev/null </dev/null

