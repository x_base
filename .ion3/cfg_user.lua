
dopath("mod_xinerama");
dopath("xinerama_switcher");
defbindings("WGroupWS", {
    kpress(MOD1.."Up", "xinerama_switcher_up(_)"),
    kpress(MOD1.."Down", "xinerama_switcher_down(_)"),
    kpress(MOD1.."Right", "xinerama_switcher_right(_)"),
    kpress(MOD1.."Left", "xinerama_switcher_left(_)"),
})
