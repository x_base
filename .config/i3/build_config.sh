#!/bin/sh

cd ~/.config/i3/;

if [ -e config_base ]; then
    cat config_base > config;
fi;

if [ -e config_$(hostname) ]; then
    cat config_$(hostname) >> config;
fi;
